var buffer_boolean = new Boolean();

// Finish all fading animations
function _finishAnime() {
	$('#header').stop(false, true);
	$('#contentHeadings').stop(false, true);
	$('.copyright').stop(false, true);
	$('#placeHolderImg').stop(false, true);
};

// Hide all necessary elements
function _hideAll() {
	$('#clickHeretoPlay').hide('fade' , 100);
	$('#infoOverlay').hide('fade' , 1000);
	$('#infoWrap').hide();
	$('#loadingWrap').hide();
}

// Reveal loading info
function _seeLoading() {
	$('#infoWrap').hide();
	$('#loadingWrap').show();
	$('#infoOverlay').switchClass('info' , 'loading').show('fade' , 1000);
}

// Hide loading info
function _byeLoading() {
	$('#infoOverlay').hide();
	$('#loadingWrap').hide();
}

// Reveal video info
function _seeInfo() {
	$('#loadingWrap').hide();
	$('#infoWrap').show();
	$('#infoOverlay').switchClass('loading' , 'info').show({
		effect: 'fold',
		easing: 'easeInOutQuart',
		duration: 1000,
	});
}

// Hide video info
function _byeInfo() {
	$('#infoOverlay').hide({
		effect: 'fold',
		easing: 'easeInOutQuart',
		duration: 1000,
		complete: function() {
			$('#infoWrap').hide()
		},
	});	
};

// Change raw decimal into percentage
function _toPercent(dec) {
	return Math.round(dec * 100);
};

// Change raw seconds data into X minutes, Y seconds
function _toMinutes(dec) {
	var raw_minutes = dec / 60;
		str_minutes = raw_minutes.toFixed(2).toString();
		minutes = str_minutes.split('.').shift();
		raw_seconds = str_minutes.split('.').pop();
		num_seconds = new Number(raw_seconds);
		seconds = (num_seconds * 60) / 100;
		
	return minutes + ' minutes, ' + Math.round(seconds) + ' seconds';
};

// Master fading function of all fade-in animations
function lightsUp() {
	_finishAnime();
	$('#header').fadeTo(1000 , 1);
	$('#contentHeadings').fadeTo(1000 , 1);
	$('.copyright').fadeTo(1000 , 1).css( {"color" : "#9DB0C7"} );
};

// Master fading function of all fade-out animations
function lightsDown() {
	_finishAnime();
	_byeInfo();
	$('#clickHeretoPlay').hide();
	$('#header').fadeTo(2000 , 0.2);
	$('#contentHeadings').fadeTo(2000 , 0.2);
	$('.copyright').fadeTo(1000 , 0.5).css( {"color" : "#9DB0C7"} );
};

// Reset and prepare to load another video
function unload() {
	_hideAll();
	lightsUp();
	$('#placeHolderImg').fadeTo(1000 , 1);
	$('#videosHere').switchClass('onStage' , 'onDeck' , 2000);
	buffer_boolean = new Boolean();
};

// Get time to finish for currently loaded video
function timeRemaining(data) {
	var vimeo_player = $f($('iframe')[0]);
		seconds_remaining = data.duration - data.seconds;
		
	$('#infoDuration').text(_toMinutes(seconds_remaining));
	console.log(Math.round(data.seconds) + ' seconds played'); //log
};

// Pause mode displays title and end time information as an overlay and initiates the lightsUp function
function pauseMode() {
	var vimeo_player = $f($('iframe')[0]);
		video_title = $('#placeHolderImg').attr('alt');
	
	lightsUp();		
	$('#infoWatch').text('you\'re watching:');
	$('#infoRun').text('time remaining:');
	$('#infoTitle').text(video_title);
	_seeInfo();
	console.info('pauseMode executed all lines'); //log
};

// Finish mode displays information about the video just watched and resets handlers for a replay option 
function finishMode() {
	var vimeo_player = $f($('iframe')[0]);
		video_title = $('#placeHolderImg').attr('alt');
		replay_button = $('#clickHeretoPlay').html('<strong>play again?</strong>');
	
	lightsUp();
	$('#infoWatch').text('you\'ve just watched:');
	$('#infoRun').text('runtime:');
	$('#infoTitle').text(video_title);
	vimeo_player.api('getDuration' , function(value) {
		$('#infoDuration').text(_toMinutes(value));
		_seeInfo();
	});
	$('#placeHolderImg').fadeTo(1000 , 1);
	$('#videosHere').switchClass('onStage' , 'onDeck' , 2000);
	$('#clickHeretoPlay').unbind()
		.bind('click' , function() {
			lightsDown();
			$('#placeHolderImg').fadeTo(2000 , 0);
			$('#videosHere').switchClass('onDeck' , 'onStage' , 1000 , function() {
				vimeo_player.api('play');
			});
		});
	replay_button.show();
	console.info('clickHeretoPlay button is now set to replay mode and function finishMode is complete'); //log
};

// Display information about the currently loaded video
function readyMode() {
	var vimeo_player = $f($('iframe')[0]);
		video_title = $('#placeHolderImg').attr('alt');
		start_button = $('#clickHeretoPlay').html('<strong>click to play</strong>');
		
	$('#clickHeretoPlay').bind('click' , preBuffer_animation);
	$('#infoWatch').text('Watch:');
	$('#infoRun').text('runtime:');
	$('#infoTitle').text(video_title);
	vimeo_player.api('getDuration' , function(value) {
		$('#infoDuration').text(_toMinutes(value));
		console.info(_toMinutes(value) + ' video duration calculated by function readyMode.'); //log
	});
	_seeInfo();
	start_button.show();
};

// Initiate background loading by calling 'play' to the player API, then pausing immediately after
function preLoad() {
	var vimeo_player = $f($('iframe')[0]);
	vimeo_player.addEvent('play' , function() {
		vimeo_player.api('pause');
		console.info('player is now paused per the preLoad function'); //log
	});
};

// Animation cues for when user initiates play, but the player is not yet all the way buffered 
function preBuffer_animation() {
	var vimeo_player = $f($('iframe')[0]),
		playVar = $('#loadingWrap').css('display');
		
	_seeLoading();
	$('#clickHeretoPlay').hide();
	$('#placeHolderImg').fadeTo(30000 , 0);
	console.info('preBuffer_animation has executed. Image should be fading away and progress bar should be visible'); //log
	if (buffer_boolean) {
		console.info('User has requested play and buffer threshold has ben met. Beginning play.'); //log
		_hideAll();
		_finishAnime();
		vimeo_player.addEvent('pause' , pauseMode);
		vimeo_player.addEvent('play' , lightsDown);
		vimeo_player.removeEvent('loadProgress');
		$('#videosHere').switchClass('onDeck' , 'onStage' , 1000 , function() {
			vimeo_player.api('play');
			console.info('video play command has been executed'); //log
		});
		$('#progressbar').progressbar({value: false});
		$('#loadingText').text('');
	}
};

// Buffer the player to 20% loaded before allowing the user to view the video
function preBuffer(data) {
	var vimeo_player = $f($('iframe')[0]),
		loading = Math.round(data.percent * 100),
		playVar = $('#loadingWrap').css('display');

	$('#progressbar').progressbar({value: loading * 5});
	$('#loadingText').text('buffering ..... ' + loading * 5 + '%');
	if (playVar == 'none' && loading < 20) {
		// DO NOTHING
		console.log('Loading is NOT visible ... ' + Math.round(loading) + '% loaded.'); //log
		buffer_boolean = false
	}
	else if (playVar !== 'none' && loading < 20) {
		vimeo_player.removeEvent('play');
		vimeo_player.api('pause');
		console.log('loadingWrap is visible ' + Math.round(loading) + '% loaded.'); //log
		buffer_boolean = false
	}
	else if (playVar !== 'none' && loading >= 20){
		console.info('User has requested play and buffer threshold has ben met. Beginning play.'); //log
		_hideAll();
		_finishAnime();
		vimeo_player.addEvent('pause' , pauseMode);
		vimeo_player.addEvent('play' , lightsDown);
		vimeo_player.removeEvent('loadProgress');
		$('#videosHere').switchClass('onDeck' , 'onStage' , 1000 , function() {
			vimeo_player.api('play');
			console.info('video play command has been executed'); //log
		});
		$('#progressbar').progressbar({value: false});
		$('#loadingText').text('');
		console.info(Math.round(loading) + ' % loaded and video is playing! This is the end of the preBuffer function'); //log
		buffer_boolean = true
	}
	if (loading >= 20) {
		buffer_boolean = true
	}
};

// Initiate player loaders and listeners
function playerReady(vimeo_id) {
	var frame_id = '#' + vimeo_id;
		vimeo_player = $f($(frame_id)[0]);
	
	console.info(vimeo_id + ' has been passed into the playerReady function'); //log
	$('#clickHeretoPlay').unbind('click');
	vimeo_player.addEvent('ready' , function() {
		preLoad();
		vimeo_player.addEvent('finish' , finishMode);
		vimeo_player.addEvent('loadProgress' , preBuffer);
		vimeo_player.addEvent('playProgress' , timeRemaining);
		vimeo_player.api('play');
		readyMode();
		console.info('ready event has been successfully added to the video'); //log
	});
};

// Initiate ready function on default iFrame
$(document).ready(function() {
	playerReady('showreel');
});










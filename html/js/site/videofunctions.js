// Finish all fading animations
function _finishAnime() {
	$('#header').stop(false, true);
	$('#contentHeadings').stop(false, true);
	$('.copyright').stop(false, true);
	$('#placeHolderImg').stop(false, true);
};

// Hide all necessary elements
function _hideAll() {
	$('#clickHeretoPlay').hide('fade' , 100);
	$('#infoOverlay').hide('fade' , 1000);
	$('#infoWrap').hide();
	$('#loadingWrap').hide();
}

// Reveal loading info
function _seeLoading() {
	$('#infoWrap').hide();
	$('#loadingWrap').show();
	$('#infoOverlay').switchClass('info' , 'loading').show('fade' , 1000);
}

// Hide loading info
function _byeLoading() {
	$('#infoOverlay').hide();
	$('#loadingWrap').hide();
}

// Reveal video info
function _seeInfo() {
	$('#loadingWrap').hide();
	$('#infoWrap').show();
	$('#infoOverlay').switchClass('loading' , 'info').show({
		effect: 'fold',
		easing: 'easeInOutQuart',
		duration: 1000,
	});
}

// Hide video info
function _byeInfo() {
	$('#infoOverlay').hide({
		effect: 'fold',
		easing: 'easeInOutQuart',
		duration: 1000,
		complete: function() {
			$('#infoWrap').hide()
		},
	});	
};

// Change raw decimal into percentage
function _toPercent(dec) {
	return Math.round(dec * 100);
};

// Change raw seconds data into X minutes, Y seconds
function _toMinutes(dec) {
	var raw_minutes = dec / 60;
		str_minutes = raw_minutes.toFixed(2).toString();
		minutes = str_minutes.split('.').shift();
		raw_seconds = str_minutes.split('.').pop();
		num_seconds = new Number(raw_seconds);
		seconds = (num_seconds * 60) / 100;
		
	return minutes + ' minutes, ' + Math.round(seconds) + ' seconds';
};

// Master fading function of all fade-in animations
function lightsUp() {
	_finishAnime();
	$('#header').fadeTo(1000 , 1);
	$('#contentHeadings').fadeTo(1000 , 1);
	$('.copyright').fadeTo(1000 , 1).css( {"color" : "#9DB0C7"} );
};

// Master fading function of all fade-out animations
function lightsDown() {
	_finishAnime();
	_byeInfo();
	$('#clickHeretoPlay').hide();
	$('#header').fadeTo(2000 , 0.2);
	$('#contentHeadings').fadeTo(2000 , 0.2);
	$('.copyright').fadeTo(1000 , 0.5).css( {"color" : "#9DB0C7"} );
};

// Reset and prepare to load another video
function unload() {
	_hideAll();
	lightsUp();
	$('#placeHolderImg').fadeTo(1000 , 1);
	$('iframe').hide('fade' , 2000);
};

// Get time to finish for currently loaded video
function timeRemaining(data) {
	var vimeo_player = $f($('iframe')[0]);
		seconds_remaining = data.duration - data.seconds;
		
	$('#infoDuration').text(_toMinutes(seconds_remaining));
};

// Pause mode displays title and end time information as an overlay and initiates the lightsUp function
function pauseMode() {
	var vimeo_player = $f($('iframe')[0]);
		video_title = $('#placeHolderImg').attr('alt');
	
	lightsUp();		
	$('#infoWatch').text('you\'re watching:');
	$('#infoRun').text('time remaining:');
	$('#infoTitle').text(video_title);
	_seeInfo();
};

// Finish mode displays information about the video just watched and resets handlers for a replay option 
function finishMode() {
	var vimeo_player = $f($('iframe')[0]);
		video_title = $('#placeHolderImg').attr('alt');
		replay_button = $('#clickHeretoPlay').html('<strong>play again?</strong>');
	
	lightsUp();
	$('#infoWatch').text('you\'ve just watched:');
	$('#infoRun').text('runtime:');
	$('#infoTitle').text(video_title);
	vimeo_player.api('getDuration' , function(value) {
		$('#infoDuration').text(_toMinutes(value));
		_seeInfo();
	});
	$('#placeHolderImg').fadeTo(1000 , 1);
	$('iframe').hide('fade' , 2000);
	$('#clickHeretoPlay').unbind().bind('click' , function() {
		lightsDown();
		$('#placeHolderImg').fadeTo(2000 , 0);
		$('iframe').show('fade' , 1000 , function() {
			vimeo_player.api('play');
		});
	});
	replay_button.show();
};

// Display information about the currently loaded video
function readyMode() {
	var vimeo_player = $f($('iframe')[0]);
		video_title = $('#placeHolderImg').attr('alt');
		start_button = $('#clickHeretoPlay').html('<strong>click to play</strong>');
		
	$('#clickHeretoPlay').bind('click' , function() {
		preBuffer_animation();
	});
	$('#infoWatch').text('Watch:');
	$('#infoRun').text('runtime:');
	$('#infoTitle').text(video_title);
	vimeo_player.api('getDuration' , function(value) {
		$('#infoDuration').text(_toMinutes(value));
	});
	_seeInfo();
	start_button.show();
};

// Initiate background loading by calling 'play' to the player API, then pausing immediately after
function preLoad() {
	var vimeo_player = $f($('iframe')[0]);
	vimeo_player.addEvent('play' , function() {
		vimeo_player.api('pause');
	});
};

// Animation cues for when user initiates play, but the player is not yet all the way buffered 
function preBuffer_animation() {
	_seeLoading();
	$('#clickHeretoPlay').hide();
	$('#placeHolderImg').fadeTo(30000 , 0);
};

// Buffer the player to 10% loaded before allowing the user to view the video
function preBuffer(data) {
	var vimeo_player = $f($('iframe')[0]);
		loading = Math.round((data.percent * 100) * 10);
		playVar = $('#loadingWrap').css('display');

	$('#statusText').text(loading);
	$('#progressbar').progressbar({value: loading});
	$('#loadingText').text(loading + '% buffered and counting!');
	if (playVar == 'none' && _toPercent(data.percent) < 10) {
		// DO NOTHING
	}
	else if (playVar == 'none' && _toPercent(data.percent) >= 10) {
		// DO NOTHING
	}
	else if (playVar != 'none' && _toPercent(data.percent) < 10) {
		vimeo_player.api('pause');
	}
	else if (playVar != 'none' && _toPercent(data.percent) >= 10){
		_hideAll();
		_finishAnime();
		vimeo_player.addEvent('pause' , pauseMode);
		vimeo_player.addEvent('play' , lightsDown);
		vimeo_player.removeEvent('loadProgress');
		$('iframe').show('fade' , 1000 , function() {
			vimeo_player.api('play');
		});
		$('#progressbar').progressbar({value: false});
		$('#loadingText').text('');
	}
};

// Initiate player loaders and listeners
function playerReady(vimeo_id) {
	var frame_id = '#' + vimeo_id;
		vimeo_player = $f($(frame_id)[0]);
	
	$('#clickHeretoPlay').unbind();
	vimeo_player.addEvent('ready' , function() {
		preLoad();
		vimeo_player.addEvent('finish' , finishMode);
		vimeo_player.addEvent('loadProgress' , preBuffer);
		vimeo_player.addEvent('playProgress' , timeRemaining);
		vimeo_player.api('play');
		readyMode();
	});
};

// Initiate ready function on default iFrame
$(document).ready(function() {
	playerReady('showreel');
});










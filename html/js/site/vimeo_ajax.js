var url = 'http://vimeo.com/api/v2/nickdaviesdp/videos.json',
	vUrl = 'http://player.vimeo.com/video/',
	callBack = '?callback=?',
	styling = '?color=5689dd&amp;api=1&amp;player_id=',
	videos = [];
	
$.getJSON(url + callBack, function(nddp) {
	for (var i = 0; i < nddp.length; i++) 
	{
		var thumb = nddp[i].thumbnail_large,
			id = nddp[i].id,
			title = nddp[i].title,
			thumb_unique_id = 'thumb_' + title.split(' ').join('').replace(/\W/g , '');	
						
	//create the thumbnail as a list item
		if (nddp[i].embed_privacy != "nowhere") {
			$('ul.vimeoThumbs').append('<li class="thumb" id="' + thumb_unique_id + '" >' + '<div class="thumbOverlay">' + '<h3>' + title + '</h3>' + '</div>' + '<img src="' + thumb + '" width="220" height="124" alt="Thumbnail of' + title + '" />' + '</li>');
			$('.thumbOverlay').hide();
			var video = {
				'title': nddp[i].title,
				'id': nddp[i].title.split(' ').join('').replace(/\W/g , ''),
				'identifier': 'thumb_' + nddp[i].title.split(' ').join('').replace(/\W/g , ''), 
				'image': nddp[i].thumbnail_large,
				'url': vUrl + nddp[i].id + styling + nddp[i].title.split(' ').join('').replace(/\W/g , '')
			};
			videos.push(video);
		}
	}
		
	//load videos to play based on thumbnail identification	
	var activeThumb = $('ul.vimeoThumbs li');
		iframeDiv = $('#videosHere');
		loadedImg = $('#placeHolderImg');
		
			
	$(activeThumb).bind('click' , function () {
		for (n = 0; n < videos.length; n++) {
			if ($(this).attr('id') == videos[n].identifier) {
				unload();
				loadedImg.attr({
					'src' : videos[n].image,
					'alt' : videos[n].title,
				});
				iframeDiv.html('<iframe id="' + videos[n].id + '" src="' + videos[n].url + '" width="682" height="384" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
				playerReady(videos[n].id);
			}
		}
	});		
});